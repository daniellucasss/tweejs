const expect = require('chai').expect
const crypter = require('../utils/crypter')
const bcrypt = require('bcrypt')

describe('Crypter tests', () => {
  it('encrypted string should match with normal string', async () => {
    let notEncryptedString = 'testing'
    let encryptedString = await crypter.hashParam(notEncryptedString)
    let res = bcrypt.compareSync(notEncryptedString, encryptedString)
    expect(res).to.be.true
  })
})