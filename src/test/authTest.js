const chai = require('chai')
const chaiHttp = require('chai-http')
const sinon = require('sinon')
const proxyquire = require('proxyquire').noCallThru()
const expect = chai.expect
const setup = require('../server/setup')
let authUtils = require('../utils/auth')

chai.use(chaiHttp)

describe('Auth tests', () => {
  let usernameTest = 'test'

  it('should gets unauthorized', () => {
    chai.request(setup)
      .get('/home').end((err, res) => {
        expect(res).to.have.status(401)
      })
  })

  it('should gets bad request error trying to login without username and password', () => {
    chai.request(setup)
      .post('/login')
      .type('form')
      .end((err, res) => {
        expect(res).to.have.status(400)
      })
  })

  it('should unauthorize when request having an invalid token', () => {
    let token = 'testingtokenwhodoesntmakeanysense'
    chai.request(setup)
      .get('/home')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        if(err) return err
        expect(res).to.have.status(401)
      })
  })

  it('should return invalid when pass old date', () => {
    let result = authUtils.isValidDate(new Date('01/01/2000'))
    expect(result).to.be.false
  })

})