const expect = require('chai').expect
const bcrypt = require('bcrypt')
const proxyquire = require('proxyquire').noCallThru()
const sinon = require('sinon')

describe('User tests', () => {
  let userUtils

  it('should throw an especific error when user isn`t found', async() => {
    userUtils = proxyquire('../utils/user', { '../db/user': {
      findOne: (arg) => (undefined)
    }})
    const user = { username: 'idontexist', password: 'meneither' }
    try {
      await userUtils.returnUserIfIsValid(user.username, user.password)
    } catch(err) {
      expect(err.message).to.be.equal('User not found')
    }
  })

  it('should throw an especific error when passwords not matching', async() => {
    userUtils = proxyquire('../utils/user', { '../db/user': {
      findOne: (arg) => { return { password: 'savedpassword' } }
    }})
    const user = { username: 'existentuser', password: 'ishouldnotmatchwiththepasswordsaved'}
    try {
      await userUtils.returnUserIfIsValid(user.username, user.password)
    } catch(err) {
      expect(err.message).to.be.equal('Passwords not matching')
    }
  })

})
