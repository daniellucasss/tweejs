const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
const setup = require('../server/setup')

chai.use(chaiHttp)

describe('Route tests', () => {
  it('should gets success', () => {
    chai.request(setup)
      .get('/').end((err, res) => {
        expect(res).to.have.status(200)
      })
  })
})
