const passport = require('passport')
const bodyParser = require('body-parser')
const app = require('./index')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())

require('../config')

module.exports = app