const app = require('./setup')
const logger = require('../config/logger')

app.listen(3000, () => {
  console.log('Running')
  logger.info(`Server started at ${new Date()}`)
})