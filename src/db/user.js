const mongoose = require('./mongoose')
const crypter = require('../utils/crypter')
const logger = require('../config/logger')

const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true }
})

UserSchema.pre('save', async function(next) {
  let user = this
  try {
    user.password = await crypter.hashParam(user.password)
  } catch(err) {
    logger.error(`Error ${err} trying to encrypt password.`)
  } finally {
    next()
  }
})

module.exports = mongoose.model('User', UserSchema)