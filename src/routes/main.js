const router = require('express').Router()
const passport = require('passport')

router.get('/', (req, res) => {
  res.send('yatta!').status(200)
})

router.post('/login', (req, res, next) => {
  if(!req.body.username || !req.body.password) {
    return res.status(400).send('error')
  }
  passport.authenticate('local', (user) => {
    res.cookie('token', user)
    return next(user)
  })(req, res, next)
})

module.exports = router