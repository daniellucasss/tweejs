const app = require('../server')
const main = require('./main')
const user = require('./user')
const home = require('./home')

app.use('/', main)
app.use('/user', user)
app.use('/home', home)