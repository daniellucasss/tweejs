const bcrypt = require('bcrypt')

async function returnUserIfIsValid(username, password) {
  const User = require('../db/user')
  let user = await User.findOne({ username })
  if(user) {
    let isValidPassword = await bcrypt.compare(password, user.password)
    if(isValidPassword) {
      return user
    }
    throw new Error('Passwords not matching')
  }
  throw new Error('User not found')
}

module.exports = {
  returnUserIfIsValid
}