const jwtSimple = require('jwt-simple')
const constants = require('../utils/constants')
const userUtils = require('./user')
const moment = require('moment')

function returnEncodedPayload(payload) {
  return jwtSimple.encode(payload, constants.JWT_KEY)
}

function isValidDate(date) {
  let fiveMinutesAgo = moment().subtract(5, 'minutes').toDate()
  let fiveMinutesForward = moment().add(5, 'minutes').toDate()
  let isValid = moment(date).isBetween(fiveMinutesAgo, fiveMinutesForward)
  return isValid
}

async function isValidPayload(payload) {
  let user = await userUtils
    .returnUserIfIsValid(payload.user.username, payload.user.password)
  if(user) {
    return this.isValidDate(payload.date)
  }
  return false
}

module.exports = {
  returnEncodedPayload,
  isValidDate,
  isValidPayload
}