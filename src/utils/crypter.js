const bcrypt = require('bcrypt')
const constants = require('./constants')

async function hashParam(param) {
  const salt = await bcrypt.genSalt(constants.ROUNDS);
  const hash = await bcrypt.hash(param, salt);
  return hash;
}

module.exports = {
  hashParam
}