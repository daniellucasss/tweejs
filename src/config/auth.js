const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const LocalStrategy = require('passport-local').Strategy
const constants = require('../utils/constants')
const authUtils = require('../utils/auth')
const userUtils = require('../utils/user')

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: constants.JWT_KEY
}

passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
  if(jwt_payload) {
    try {
      let payload = await authUtils.isValidPayload(jwt_payload)
      return done(null, jwt_payload)
    } catch(err) {
      return done(err)
    }
  }
  return done(null, false)
}))

passport.use(new LocalStrategy(async (username, password, done) => {
  if(username && password) {
    try {
      let user = await userUtils.returnUserIfIsValid(username, password)
      let token = authUtils.returnEncodedPayload({
        user: { username, password},
        date: new Date() 
      })
      return done(token, true)
    } catch(err) {
      return done(err)
    }
  }
  return done(null, false)
}))
