const log4js = require('log4js')

log4js.configure({
  appenders: {
    'tweejs': {
      'type': 'file',
      'filename': `logs/log ${new Date().toDateString()}.log`
    }
  },
  categories: {
    default: {
      appenders: ['tweejs'],
      level: 'trace'
    }
  }
})

const logger = log4js.getLogger('tweejs')

module.exports = logger